#!/bin/bash
function list() {
  curl --request GET -sL \
    --url 'http://localhost/payments/list' \
    --output /dev/null
  echo "list"
}

function detail() {
  id=$((RANDOM % 10))

  curl --request GET -sL \
    --url "http://localhost/payments/detail?id=$id" \
    --output /dev/null
  echo "detail"
}

function create() {
  amount=$((RANDOM % 100))
  received=$((amount + RANDOM % 100))
  curl --request POST -sL \
    --url "http://localhost/payments/create" \
    --form "amount=$amount" \
    --form "received=$received" \
    --output /dev/null
  echo "create"
}

while true; do
  random=$(($RANDOM % 3))
  case $random in
  0)
    list
    ;;
  1)
    detail
    ;;
  2)
    create
    ;;
  esac

  sleep 3
done
