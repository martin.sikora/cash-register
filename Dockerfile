# Install composer dependencies
FROM composer:2 as vendor
COPY composer.* .
RUN composer install --ignore-platform-reqs

# Create server
FROM php:8.0-apache as server

# Allow .htacess rewrite mode
RUN a2enmod rewrite

# Change apache's default document root folder via symlink
RUN rm -rf /var/www/html && ln -s /app/src/public /var/www/html

# Install extensions
RUN docker-php-ext-install pdo_mysql

# Copy source files
WORKDIR /app
COPY src/ ./src/
COPY --from=vendor /app/vendor/ ./vendor/
