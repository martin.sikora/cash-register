<?php

namespace MartinSikora\CashRegister;

use MartinSikora\CashRegister\controllers\ApiControllerInterface;
use MartinSikora\CashRegister\Controllers\DocsController;
use MartinSikora\CashRegister\Controllers\ErrorController;
use MartinSikora\CashRegister\Controllers\PaymentController;
use MartinSikora\CashRegister\Entities\Payment\PaymentRepository;

/**
 * Class responsible for handling request and response.
 */
class Router
{
    const ERROR_CONTROLLER_CLASS = ErrorController::class;

    protected Request $request;
    protected Response $response;
    protected Database $database;

    public function __construct(Request $request, Response $response, Database $database)
    {
        $this->request = $request;
        $this->response = $response;
        $this->database = $database;
    }

    /**
     * Parse HTTP request and create controller associated with requested action or error controller for unknown route.
     * @return array Two items array with controller object and action name.
     */
    public function parseRequest(): array
    {
        [$controllerName, $actionName] = $this->request->parseRequest();
        $controller = $this->createController($controllerName);
        return [$controller, $actionName];
    }

    protected function createController(string $controllerName): ApiControllerInterface
    {
        $controllerClassName = $this->getControllerClassName($controllerName);
        $controllerRepositoryClassName = $this->getControllerRepositoryClassName($controllerName);
        if ($controllerRepositoryClassName) {
            $repository = new $controllerRepositoryClassName($this->database);
            return new $controllerClassName($this->request, $this->response, $repository);
        }
        return new $controllerClassName($this->request, $this->response);
    }

    /**
     * Returns namespace of controller by URL name or ErrorController for unknown name.
     *
     * @param string $controllerName controller URL name
     * @return string controller's namespace
     */
    protected function getControllerClassName(string $controllerName): string
    {
        return $this->getControllersMapping()[$controllerName]['controller'] ?? self::ERROR_CONTROLLER_CLASS;
    }

    protected function getControllerRepositoryClassName(string $controllerName): string
    {
        return $this->getControllersMapping()[$controllerName]['repository'] ?? '';
    }

    /**
     * @return array controller name from URL mapped to class
     */
    protected function getControllersMapping(): array
    {
        return [
            'payments' => ['controller' => PaymentController::class, 'repository' => PaymentRepository::class],
            'docs' => ['controller' => DocsController::class, 'repository' => null],
        ];
    }

    public function getLogMessage(): string
    {
        $date = date('Y-m-d H:i:s');
        $requestLogInfo = $this->request->getLogMessage();
        $responseLogInfo = $this->response->getLogMessage();
        return "{$date}|{$requestLogInfo}{$responseLogInfo}";
    }
}
