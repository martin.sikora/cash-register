<?php

namespace MartinSikora\CashRegister;

/**
 * Class responsible for handling HTTP request.
 */
class Request
{
    const URL_SEPARATOR = '/';

    /**
     * @return array Request body data.
     */
    public function getBodyData(): array
    {
        return $_POST;
    }

    /**
     * Returns value of the single body parameter. Cast it to int or float if it's possible.
     *
     * @param string $name requested body parameter name
     * @param mixed|null $defaultValue value returned if requested param does not exist
     * @return string|integer|null|float requested body param value
     */
    public function getBodyParam(string $name, mixed $defaultValue = null): null|string|int|float
    {
        $floatValue = filter_input(INPUT_POST, $name, FILTER_VALIDATE_FLOAT);
        if (is_numeric($floatValue)) {
            return $floatValue;
        }

        $intValue = filter_input(INPUT_POST, $name, FILTER_VALIDATE_INT);
        if (is_numeric($intValue)) {
            return $intValue;
        }

        return $this->getBodyData()[$name] ?? $defaultValue;
    }

    /**
     * Parse HTTP request and return requested controller and action names.
     * URL example: example.com/controllerName/actionName
     *
     * @return string[] Two items array with controller name and action name.
     */
    public function parseRequest(): array
    {
        $url = preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI']);
        $requestURI = trim($url, self::URL_SEPARATOR);
        [$controllerName, $actionName] = array_pad(explode(self::URL_SEPARATOR, $requestURI), 2, '');
        return [$controllerName, $actionName];
    }

    public function getUri(): string
    {
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * @return array Request query params.
     */
    public function getQueryParams(): array
    {
        return $_GET;
    }

    /**
     * Returns value of the single body parameter.
     *
     * @param string $parameter Requested query parameter name.
     * @param string $defaultValue Value returned if requested param does not exist.
     * @return string Requested query param value.
     */
    public function getQueryParam(string $parameter, string $defaultValue = ''): string
    {
        return $this->getQueryParams()[$parameter] ?? $defaultValue;
    }

    /**
     * @return string Requested HTTP method.
     */
    public function getHttpMethod(): string
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public function getLogMessage(): string
    {
        $httpMethod = $this->getHttpMethod();
        $uri = $this->getUri();
        $body = json_encode($this->getBodyData());
        return "{$httpMethod}|{$uri}|{$body}|";
    }
}
