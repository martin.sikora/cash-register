<?php

namespace MartinSikora\CashRegister;

use PDO;

class Database
{
    protected PDO $connection;

    public function __construct(string $driver, string $host, string $dbname, string $username, string $password)
    {
        $dsn = "{$driver}:dbname={$dbname};host={$host}";
        $this->connection = new PDO($dsn, $username, $password);
    }

    public function getConnection(): PDO
    {
        return $this->connection;
    }
}
