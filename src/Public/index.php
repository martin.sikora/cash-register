<?php

use MartinSikora\CashRegister\{App, Database, Env, Response, Request, Router};

require __DIR__ . '/../../vendor/autoload.php';

Env::init(__DIR__ . '/../../.env');

$dbDriver = Env::get(Env::DB_DRIVER, '');
$dbHost = Env::get(Env::DB_HOST, '');
$dbName = Env::get(Env::DB_NAME, '');
$dbUser = Env::get(Env::DB_USER, '');
$dbPassword = Env::get(Env::DB_PASSWORD, '');
$database = new Database($dbDriver, $dbHost, $dbName, $dbUser, $dbPassword);

$request = new Request();
$response = new Response();
$router = new Router($request, $response, $database);

$app = new App($router);
$app->run();
