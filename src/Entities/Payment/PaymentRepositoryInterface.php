<?php

namespace MartinSikora\CashRegister\Entities\Payment;

use MartinSikora\CashRegister\Entities\RepositoryInterface;

interface PaymentRepositoryInterface extends RepositoryInterface
{
    /**
     * @param PaymentModel $model
     * @return bool
     */
    public function add(PaymentModel $model): PaymentModel;

    /**
     * @return PaymentModel[]
     */
    public function getAll(): array;

    /**
     * @param int $id
     * @return PaymentModel|null
     */
    public function findById(int $id): ?PaymentModel;
}