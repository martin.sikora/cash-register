<?php

namespace MartinSikora\CashRegister\Entities\Payment;

use Exception;
use MartinSikora\CashRegister\Entities\BaseRepository;
use PDO;

class PaymentRepository extends BaseRepository implements PaymentRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function getTableName(): string
    {
        return 'payment';
    }

    /**
     * @inheritDoc
     */
    public function add(PaymentModel $model): PaymentModel
    {
        if (!$model->validate()) {
            return $model;
        }

        $tableName = $this->getTableName();
        $query = "INSERT INTO {$tableName} (`amount`, `received`, `returned`) VALUES (:amount, :received, :returned)";

        $statement = $this->database
            ->getConnection()
            ->prepare($query);

        $params = [
            ':amount' => $model->amount,
            ':received' => $model->received,
            ':returned' => $model->returned,
        ];

        if ($statement === false || !$statement->execute($params)) {
            throw new Exception('database error');
        }

        return $this->findById($this->database->getConnection()->lastInsertId());
    }


    /**
     * @inheritDoc
     */
    public function getAll(): array
    {
        $tableName = $this->getTableName();
        $sql = "SELECT * FROM {$tableName};";
        $statement = $this->database
            ->getConnection()
            ->prepare($sql);

        if ($statement === false || !$statement->execute()) {
            return [];
        }

        return $statement->fetchAll(PDO::FETCH_CLASS, PaymentModel::class);
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id): ?PaymentModel
    {
        $tableName = $this->getTableName();
        $sql = "SELECT * FROM `{$tableName}` WHERE `id` = :id";
        $statement = $this->database
            ->getConnection()
            ->prepare($sql);
        $statement->bindValue(':id', $id);

        if ($statement === false || !$statement->execute()) {
            return null;
        }

        $object = $statement->fetchObject(PaymentModel::class);
        return $object === false ? null : $object;
    }
}