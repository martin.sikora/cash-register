<?php

namespace MartinSikora\CashRegister\Entities\Payment;

use MartinSikora\CashRegister\Entities\BaseModel;
use TypeError;

/**
 * @OA\Schema(
 *     schema="paymentSchema",
 *     type="object",
 *     @OA\Property(property="id", type="integer", example=17),
 *     @OA\Property(property="amount", type="float", example=100.50),
 *     @OA\Property(property="received", type="float", example=200.00),
 *     @OA\Property(property="returned", type="float", example=99.50),
 *     @OA\Property(property="created", type="float", example="2021-06-25 08:32:11"),
 * ),
 *
 * @OA\Schema(
 *     schema="paymentCreateSchema",
 *     type="object",
 *     @OA\Property(property="amount", type="float", example=100.50),
 *     @OA\Property(property="received", type="float", example=200.00),
 * ),
 */
class PaymentModel extends BaseModel
{
    public ?int $id = null;
    public ?float $amount = null;
    public ?float $received = null;
    public ?float $returned = null;
    public ?string $created = null;

    public function getAttributes(): array
    {
        return [
            'id' => $this->id,
            'amount' => $this->amount,
            'received' => $this->received,
            'returned' => $this->returned,
            'created' => $this->created,
        ];
    }

    public function calculateReturn()
    {
        $this->returned = $this->received - $this->amount;
    }

    public function validate(): bool
    {
        return $this->validateAmount()
            && $this->validateReceived()
            && $this->validateReturned();
    }

    protected function validateAmount(): bool
    {
        if (is_null($this->amount)) {
            $this->setValidationError('`amount` is required and it has to be float.');
            return false;
        }

        if ($this->amount <= 0) {
            $this->setValidationError('`amount` has to be positive number.');
            return false;
        }

        return true;
    }

    protected function validateReceived(): bool
    {
        if (is_null($this->received)) {
            $this->setValidationError('`received` is required and it has to be float.');
            return false;
        }

        if ($this->received <= 0) {
            $this->setValidationError('`received` has to be positive number.');
            return false;
        }

        if ($this->received < $this->amount) {
            $this->setValidationError('`received` has to be greater or equal to `amount`.');
            return false;
        }

        return true;
    }

    protected function validateReturned(): bool
    {
        if (is_null($this->returned)) {
            $this->setValidationError('`returned` is required and it has to be float.');
            return false;
        }

        if ($this->returned < 0) {
            $this->setValidationError('`returned` has to be positive number or `0`.');
            return false;
        }

        return true;
    }
}
