<?php

namespace MartinSikora\CashRegister\Entities;

interface RepositoryInterface
{
    /**
     * @return string
     */
    public function getTableName(): string;
}
