<?php

namespace MartinSikora\CashRegister\Entities;

use TypeError;

abstract class BaseModel implements ModelInterface, ValidatableInterface
{
    use Validatable;

    public function setAttributes(array $data): void
    {
        try {
            foreach ($data as $param => $value) {
                $this->{$param} = $value;
            }
        } catch (TypeError) {
            $this->setValidationError('bad data type');
            return;
        }
    }
}