<?php

namespace MartinSikora\CashRegister\Entities;

use MartinSikora\CashRegister\Database;

abstract class BaseRepository implements RepositoryInterface
{
    protected Database $database;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }
}