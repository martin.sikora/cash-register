<?php

namespace MartinSikora\CashRegister\Entities;

interface ModelInterface
{
    public function getAttributes(): array;
}
