<?php

namespace MartinSikora\CashRegister\Entities;

trait Validatable
{
    protected array $validationErrors = [];

    public function hasError(): bool
    {
        return !empty($this->getValidationErrors());
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function setValidationError(string $error): void
    {
        $this->validationErrors[] = $error;
    }
}
