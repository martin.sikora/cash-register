<?php

namespace MartinSikora\CashRegister\Entities;

interface ValidatableInterface
{
    public function validate(): bool;

    public function hasError(): bool;

    public function getValidationErrors(): array;

    public function setValidationError(string $error): void;
}
