<?php

namespace MartinSikora\CashRegister;

/**
 * Class responsible for info message logging.
 */
class Log
{
    const INFO_LOG_FILE_PATH = __DIR__ . '/Runtime/info.log';

    private static ?Log $instance = null;

    public static function getInstance(): Log
    {
        if (is_null(self::$instance)) {
            self::$instance = new Log();
        }

        return self::$instance;
    }

    /**
     * Logs new message to info log file.
     * @param string $message
     */
    public function infoLog(string $message): void
    {
        $fileHandler = fopen(self::INFO_LOG_FILE_PATH, 'a');
        fwrite($fileHandler, "{$message}\n");
        fclose($fileHandler);
    }
}
