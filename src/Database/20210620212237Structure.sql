CREATE TABLE `payment`
(
    `id`       INT UNSIGNED            NOT NULL AUTO_INCREMENT,

    `amount`   DECIMAL(10, 2) UNSIGNED NOT NULL,
    `received` DECIMAL(10, 2) UNSIGNED NOT NULL,
    `returned` DECIMAL(10, 2) UNSIGNED NOT NULL,

    `created`  DATETIME                NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (`id`)
)
