<?php

use MartinSikora\CashRegister\Database;
use MartinSikora\CashRegister\Env;

require __DIR__ . '/../../vendor/autoload.php';

Env::init(__DIR__ . '/../../.env');

$dbDriver = Env::get(Env::DB_DRIVER, '');
$dbHost = Env::get(Env::DB_HOST, '');
$dbName = Env::get(Env::DB_NAME, '');
$dbUser = Env::get(Env::DB_USER, '');
$dbPassword = Env::get(Env::DB_PASSWORD, '');
$database = new Database($dbDriver, $dbHost, $dbName, $dbUser, $dbPassword);

$migrationPath = __DIR__;
$migrationsFileNames = scandir(__DIR__);
foreach ($migrationsFileNames as $migrationsFileName) {
    if (!preg_match('/\.sql$/', $migrationsFileName)) {
        continue;
    }
    $sql = file_get_contents("{$migrationPath}/{$migrationsFileName}");
    $database->getConnection()->prepare($sql)->execute();
}
