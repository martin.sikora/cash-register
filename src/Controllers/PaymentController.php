<?php

namespace MartinSikora\CashRegister\Controllers;

use MartinSikora\CashRegister\Entities\Payment\PaymentModel;
use MartinSikora\CashRegister\Entities\Payment\PaymentRepositoryInterface;
use MartinSikora\CashRegister\Request;
use MartinSikora\CashRegister\Response;

class PaymentController extends BaseController
{
    public PaymentRepositoryInterface $paymentRepository;

    public function __construct(Request $request, Response $response, PaymentRepositoryInterface $paymentRepository)
    {
        parent::__construct($request, $response);
        $this->paymentRepository = $paymentRepository;
    }

    /**
     * @OA\Get(
     *     path="/payments/list",
     *     tags={"Payments"},
     *
     *     @OA\Response(
     *       response=200,
     *       description="List of payments",
     *       @OA\JsonContent(
     *           @OA\Property(
     *              property="items",
     *              title="",
     *              type="array",
     *              @OA\Items(
     *                  ref="#/components/schemas/paymentSchema",
     *              ),
     *          ),
     *       ),
     *     ),
     *
     *     @OA\Response(
     *       response=405,
     *       description="Bad HTTP method",
     *     ),
     * ),
     */
    public function list(): Response
    {
        if ($this->request->getHttpMethod() !== 'GET') {
            return $this->response
                ->setStatusCode(405);
        }

        $data = $this->paymentRepository->getAll();
        $data = array_map(fn(PaymentModel $model) => $model->getAttributes(), $data);
        return $this->response->setBody([
            'items' => $data,
        ]);
    }


    /**
     * @OA\Post (
     *     path="/payments/create",
     *     tags={"Payments"},
     *
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  ref="#/components/schemas/paymentCreateSchema"
     *              )
     *          ),
     *     ),
     *
     *     @OA\Response(
     *       response=201,
     *       description="Successfully created",
     *       @OA\JsonContent(
     *          type="array",
     *          @OA\Items(
     *              ref="#/components/schemas/paymentSchema",
     *          ),
     *       ),
     *     ),
     *
     *     @OA\Response(
     *       response=405,
     *       description="Bad HTTP method",
     *     ),
     *
     *     @OA\Response(
     *       response=422,
     *       description="Unprocessable data",
     *       @OA\JsonContent(
     *          @OA\Property(
     *              property="error",
     *              title="",
     *              type="string",
     *              example="query param `id` needs to be integer"
     *          ),
     *       ),
     *     ),
     * ),
     */
    public function create(): Response
    {
        if ($this->request->getHttpMethod() !== 'POST') {
            return $this->response
                ->setStatusCode(405);
        }

        $payment = new PaymentModel();
        $safeAttributes = ['amount', 'received'];
        $values = [];
        foreach ($safeAttributes as $param) {
            $values[$param] = $this->request->getBodyParam($param);
        }
        $payment->setAttributes($values);
        $payment->calculateReturn();

        $model = $this->paymentRepository->add($payment);
        if ($model->hasError()) {
            return $this->response
                ->setStatusCode(422)
                ->setBody(['errors' => $model->getValidationErrors()]);
        }

        return $this->response
            ->setStatusCode(201)
            ->setBody($model->getAttributes());
    }

    /**
     * @OA\Get(
     *     path="/payments/detail",
     *     tags={"Payments"},
     *
     *     @OA\Parameter(
     *         name="id",
     *         in="query",
     *         description="Payment ID",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *         ),
     *         style="form"
     *     ),
     *
     *     @OA\Response(
     *       response=200,
     *       description="List of payments",
     *       @OA\JsonContent(
     *          type="array",
     *          @OA\Items(
     *              ref="#/components/schemas/paymentSchema",
     *          ),
     *       ),
     *     ),
     *
     *     @OA\Response(
     *       response=404,
     *       description="Payment does not exist",
     *       @OA\JsonContent(
     *          @OA\Property(
     *              property="error",
     *              title="",
     *              type="string",
     *              example="Entity not found"
     *          ),
     *       ),
     *     ),
     *
     *     @OA\Response(
     *       response=405,
     *       description="Bad HTTP method",
     *     ),
     *
     *     @OA\Response(
     *       response=422,
     *       description="Unprocessable data",
     *       @OA\JsonContent(
     *          @OA\Property(
     *              property="error",
     *              title="",
     *              type="string",
     *              example="query param `id` needs to be integer"
     *          ),
     *       ),
     *     ),
     * ),
     */
    public function detail(): Response
    {
        if ($this->request->getHttpMethod() !== 'GET') {
            return $this->response
                ->setStatusCode(405);
        }

        $id = $this->request->getQueryParam('id');
        if (!is_numeric($id)) {
            return $this->response->setStatusCode(422)->setBody([
                'error' => 'query param `id` needs to be integer',
            ]);
        }
        $model = $this->paymentRepository->findById($id);
        if (!$model) {
            return $this->response
                ->setStatusCode(404)
                ->setBody([
                    'error' => 'Entity not found',
                ]);
        }

        return $this->response->setBody($model->getAttributes());
    }
}