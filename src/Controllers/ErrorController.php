<?php

namespace MartinSikora\CashRegister\Controllers;

/**
 * Controller with error actions.
 */
class ErrorController extends BaseController
{
    public string $defaultActionName = 'notFound';

    public function notFound()
    {
        return $this->response
            ->setStatusCode(404)
            ->setBody([
                'error' => 'Endpoint not found',
            ]);
    }
}
