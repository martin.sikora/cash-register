<?php

namespace MartinSikora\CashRegister\controllers;

use MartinSikora\CashRegister\Response;

interface ApiControllerInterface
{
    /**
     * Runs action by name and prepare response.
     *
     * @param string $actionName
     * @return Response
     */
    public function runAction(string $actionName): Response;
}