<?php

namespace MartinSikora\CashRegister\Controllers;

use MartinSikora\CashRegister\Request;
use MartinSikora\CashRegister\Response;

/**
 * Base controller of every API controller.
 *
 * @OA\Info(
 *   version="0.1.0",
 *   title="API",
 * ),
 *
 * @OA\Server(
 *   url="/",
 *   description="This",
 * ),
 */
abstract class BaseController implements ApiControllerInterface
{
    protected string $defaultActionName = 'list';

    protected Request $request;
    protected Response $response;

    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * @inheritDoc
     */
    public function runAction(string $actionName): Response
    {
        if ($actionName === '') {
            $actionName = $this->defaultActionName;
        }

        if (method_exists($this, $actionName)) {
            return call_user_func([$this, $actionName]);
        }

        return $this->response
            ->setStatusCode(404)
            ->setBody(['error' => 'Endpoint not found.']);
    }

    /**
     * Renders specified HTML file.
     *
     * @param string $filePath Relative path from /src/Views directory.
     * @return string HTML content.
     */
    public function renderStaticHtml(string $filePath): string
    {
        $filePath = ltrim($filePath, '/');
        $viewsPath = __DIR__ . '/../Views';
        return file_get_contents("{$viewsPath}/{$filePath}");
    }
}
