<?php

namespace MartinSikora\CashRegister\Controllers;

use MartinSikora\CashRegister\Response;

class DocsController extends BaseController
{
    public string $defaultActionName = 'index';

    public function index(): Response
    {
        $html = $this->renderStaticHtml('Docs/index.html');
        return $this->response
            ->setHtmlResponseType()
            ->setBodyString($html);
    }
}
