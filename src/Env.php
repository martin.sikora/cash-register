<?php

namespace MartinSikora\CashRegister;

/**
 * Class responsible for handling environment variables.
 */
class Env
{
    const DB_DRIVER = 'DB_DRIVER';
    const DB_HOST = 'DB_HOST';
    const DB_NAME = 'DB_NAME';
    const DB_USER = 'DB_USER';
    const DB_PASSWORD = 'DB_PASSWORD';

    /**
     * @var array loaded variables
     */
    private static array $variables = [];

    /**
     * Loads variables from environment config file.
     *
     * @param string $filePath Path to the config file.
     */
    public static function init(string $filePath)
    {
        static::$variables = static::getVariablesFromFile($filePath);
    }

    /**
     * Loads and parse file and returns variables as associative array.
     *
     * @param string $filePath Path to the config file.
     * @return array parsed variables
     */
    protected static function getVariablesFromFile(string $filePath): array
    {
        $fileContent = static::loadFile($filePath);
        return static::parseFileContent($fileContent);
    }

    /**
     * Parse environment config file content and returns variables as associative array.
     *
     * @param string $fileContent Environment config file content.
     * @return array parsed variables
     */
    protected static function parseFileContent(string $fileContent): array
    {
        $variables = [];

        $fileRows = explode("\n", $fileContent);
        foreach ($fileRows as $fileRow) {
            if (trim($fileRow) == '') {
                // skip empty lines
                continue;
            }

            [$name, $value] = explode('=', $fileRow, 2);
            $variables[$name] = $value;
        }

        return $variables;
    }

    /**
     * Returns content of the file.
     *
     * @param string $filePath
     * @return string
     */
    protected static function loadFile(string $filePath): string
    {
        return file_get_contents($filePath);
    }

    /**
     * Returns value of the single environment variable.
     *
     * @param string $name variable name
     * @param mixed|null $defaultValue Returned value if variable does not exist.
     * @return mixed
     */
    public static function get(string $name, mixed $defaultValue = null): mixed
    {
        return static::$variables[$name] ?? $defaultValue;
    }
}
