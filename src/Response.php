<?php

namespace MartinSikora\CashRegister;

/**
 * Class responsible for setting HTTP response.
 */
class Response
{
    const TYPE_JSON = 'json';
    const TYPE_HTML = 'html';

    public string $type = self::TYPE_JSON;

    /**
     * @var int HTTP status code for response
     */
    protected int $statusCode = 200;

    /**
     * @var string Body params for response
     */
    protected string $body = '';

    /**
     * @param array $body
     * @return $this
     */
    public function setBody(array $body): Response
    {
        $this->body = json_encode($body);
        return $this;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function setBodyString(string $body): Response
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @param int $statusCode
     * @return $this
     */
    public function setStatusCode(int $statusCode): Response
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * Sets proper headers, status code and sends body param.
     */
    public function send(): void
    {
        http_response_code($this->statusCode);
        $this->setHeaders();
        echo $this->body;
    }

    /**
     * Sets response headers based on response type.
     */
    protected function setHeaders(): void
    {
        $headers = match ($this->type) {
            self::TYPE_JSON => ['Content-Type: application/json'],
            self::TYPE_HTML => ['Content-Type: text/html'],
        };

        foreach ($headers as $header) {
            header($header);
        }
    }

    public function getLogMessage(): string
    {
        $body = match ($this->type) {
            self::TYPE_JSON => $this->body,
            self::TYPE_HTML => 'html content skipped',
        };

        return "{$this->statusCode}|{$body}";
    }

    public function setHtmlResponseType(): Response
    {
        $this->type = self::TYPE_HTML;
        return $this;
    }
}
