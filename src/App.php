<?php

namespace MartinSikora\CashRegister;

/**
 * Main class responsible for running the app.
 */
class App
{
    protected Router $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * Parses requests, runs requested controller and sets response.
     */
    public function run(): void
    {
        [$controller, $actionName] = $this->router->parseRequest();
        $response = $controller->runAction($actionName);

        Log::getInstance()->infoLog($this->router->getLogMessage());

        $response->send();
    }
}
