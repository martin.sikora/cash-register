# Cash register

Just a demonstration project. Cash register simulator, which has 3 API endpoints (list, create, detail) for payments.

## Technologies

- PHP 8
- Composer 2.0
- MariaDB 10.6

## Development

For development there is a [Docker](https://www.docker.com/) configuration file to run app in containers.

1. Create `.env` with environment variables (see `.env.template`). You can run `make init`
1. Run `make fresh` to build and create containers.
1. Run `make run` to run containers. You can then open [localhost](http://localhost).
1. Run `make migrate-db` to set up database structure.

## Endpoint

API documentation defined via [Swagger](https://swagger.io/) is at `/docs` URL path (ex: `http://localhost/docs`).

## Logs

Application log every request and response and saves it into `src/Runtime/info.log`.

Structure of the log message is: `timestamp`|`HTTP method`|`Requested URI`|`Body params`|`Response code`|`Response body`

for example:
```
2021-06-25 07:57:17|GET|/payments/detail?id=9|[]|200|{"id":9,"amount":82,"received":87,"returned":5,"created":"2021-06-25 07:53:57"}
```

## Testing

For testing purposes there is `random-requests.sh` script which creates a random request every 3 seconds.

How to test:

1. Run `make init fresh run` to start containers (wait a moment before containers are ready)
1. Run `make migrate-db` to create database schema
1. In different terminal window run `sh random-requests.sh` to start sending random requests to server
1. Run `make app-info-log` to view requests/response logs in real time.
