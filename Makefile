help:
	@echo "Commands:"
	@echo "  init - init environment"
	@echo "  run - build, create and start containers"
	@echo "  fresh - rebuild, create and start containers"
	@echo "  stop - stop running containers"
	@echo "  clean - remove created containers"
	@echo "  console - jump to server console"
	@echo "  app-info-log - show application info logs"
	@echo "  migrate-db - run database migration script"
	@echo "  regenerate-api-docs - generate API docs"
	@echo "  clear-logs - clear app info logs"

init:
	cp .env.template .env

run:
	docker-compose up -d

fresh:
	docker-compose up -d --build --force-recreate

stop:
	docker-compose stop

clean:
	docker-compose down

console:
	docker exec -it cash_register_server /bin/bash

app-info-log:
	docker exec cash_register_server tail -f /app/src/Runtime/info.log

migrate-db:
	docker exec cash_register_server php /app/src/Database/migrate-database.php

regenerate-api-docs:
	docker exec cash_register_server php vendor/bin/openapi -e vendor -o src/Public/swagger-api-docs.json src

clear-logs:
	rm src/Runtime/info.log || true
